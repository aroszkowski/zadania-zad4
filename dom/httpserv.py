
import	socket
import	os
import	time
import	errno
import	logging
from	daemon	import	runner
from	SocketServer	import	*
import	threading


logger	=	logging.getLogger("DaemonLog")
logger.setLevel(logging.DEBUG)
formatter	=	logging.Formatter("%(asctime)s	-	%(name)s	-	%(levelname)s	-	%(message)s")
handler	=	logging.FileHandler("/home/p14/dom/demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)
_HOST	=	'194.29.175.240'
_PORT	=	31014
_ROOT_PATH	=	'/home/p14/dom/web'
page	=	"<!DOCTYPE	html><html><head><meta	charset='utf-8'/></head><body><h1>%s</h1></body></html>"
_404	=	page	%	'404	-	Not	found'
_400	=	page	%	'400	-	Bad	request'


class	http_server():
	def	make_answer(self,request_type,	content):
		result	=	''
		header	=	"HTTP/1.1	%s	%s	\r\n"
		type	=	"Content-type:	%s	\r\n"
		content_length	=	"Content-length:	%s\r\n"
		if	request_type	==	'html':
			result	+=	header	%	('200',	'OK')
			result	+=	type	%	'text/html'
		elif	request_type	==	'404':
			result	+=	header	%	('404',	'Not	found')
			result	+=	type	%	'text/html'
			logger.error('NOT	FOUND')
		elif	request_type	==	'txt':
			result	+=	header	%	('200',	'OK')
			result	+=	type	%	'text/plain;	charset=UTF-8'
		elif	request_type	==	'jpg':
			result	+=	header	%	('200',	'OK')
			result	+=	type	%	'image/jpeg'
		elif	request_type	==	'png':
			result	+=	header	%	('200',	'OK')
			result	+=	type	%	'image/png'
		elif	request_type	==	'listing':
			result	+=	header	%	('200',	'OK')
			result	+=	type	%	'text/html'
		else:
			result	+=	header	%	('400',	'Bad	request')
			result	+=	type	%	'text/html'
			logger.error('Error	BAD	REQUEST')

		result	+=	content_length	%	len(content)
		result	+=	"GMT-Date:	%s\r\n"	%	(time.strftime("%a,	%d	%b	%Y	%H:%M:%S",	time.localtime()))
		result	+=	"\r\n%s"	%	content
		return	result


	def	make_listing(self,uri):
		result	=	"<!DOCTYPE	html><html><head><meta	charset='utf-8'	/></head><body>"
		result	+=	'<h1>Listing	directory:	'	+	uri	+	'</h1><ul>'


		if	uri[-1:]	!=	'/':
			uri	+=	'/'

		listing	=	os.listdir(_ROOT_PATH+	uri)
		for	item	in	listing:
			file_path	=	uri	+	item
			if	os.path.isdir(_ROOT_PATH	+	uri	+	item):
				result	+=	'<li><a	href="'	+	file_path	+	'/">'	+	item	+	'/</a></li>'
			else:
				result	+=	'<li><a	href="'	+	file_path	+	'">'	+	item	+	'</a></li>'

		result	+=	'</ul></body></html>'
		return	result


	def	check_request(self,	request):
		header	=	request.split('\r\n')
		if	header.count	>	0:
			header	=	header[0].split(' ')
			if	header.count	>	0:
				if	header[0]	==	"GET"	and	header[2].split('/')[0]	==	"HTTP":
					return	1
		return	0
	def	handle_request(self,	sock):
		request	=	sock.recv(1024)
		if	not	request:
			return
		logger.info(sock)
		if	self.check_request(request):
			uri	=	request.split('\r\n')[0].split(' ')[1]
			logger.info("URI"+uri)
			path	=	_ROOT_PATH	+	uri
			if	os.path.exists(path):
				if	os.path.isdir(path):
					answer	=	self.make_answer('listing',	self.make_listing(uri))
				else:
					file	=	open(path,	'rb')
					if	uri[-5:]	==	'.html':
						type	=	'html'
					elif	uri[-4:]	==	'.txt':
						type	=	'txt'
					elif	uri[-4:]	==	'.jpg':
						type	=	'jpg'
					elif	uri[-4:]	==	'.png':
						type	=	'png'
					answer	=	self.make_answer(type,	file.read())
					file.close()
			else:
				answer	=	self.make_answer('404',	_404)
		else:
			answer	=	self.make_answer('400',	_400)
		logger.info(answer)
		self.handle_response(sock,answer)

	def	handle_response(self,sock,ans):
		sock.sendall(ans)
		sock.close()

httpserver	=	http_server()
class	App:

	def	__init__(self):
		self.stdin_path	=	'/dev/null'
		self.stdout_path	=	'/dev/tty'
		self.stderr_path	=	'/dev/tty'
		self.pidfile_path	=	'/home/p14/dom/daemon1.pid'
		self.pidfile_timeout	=	5

	def	run(self):
		logger.info('run	1st')
		try:
			server = MyServer((_HOST, _PORT), MyHandler)
			logger.info('..')
		except	Exception,	ex:
			logger.error(ex.message)
		try:
			logger.info('..1')
			server.serve_forever()
		except	Exception,	ex:
			logger.error(ex.message)

class MyHandler(BaseRequestHandler):

    def handle(self):
        httpserver.handle_request(self.request)


class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


    

if	__name__	==	"__main__":
	app	=	App()
	try:
		daemon_runner	=	runner.DaemonRunner(app)
	except	Exception,	ex:
		logger.error(ex.message)
	daemon_runner.daemon_context.files_preserve	=	[handler.stream]
	try:
		daemon_runner.do_action()
	except	Exception,	exc:
		logger.error(exc.message)

